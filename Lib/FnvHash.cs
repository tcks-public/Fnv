﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fnv
{
    /// <summary>
	/// Holds and allow extend hash code computed by FNV (Flowler-Noll-Vo) algorithm.
	/// </summary>
	/// <remarks>
	/// https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
	/// http://www.isthe.com/chongo/tech/comp/fnv/
	/// </remarks>
	public partial struct FnvHash : IEquatable<FnvHash> {
		/// <summary>
		/// Computed has.
		/// </summary>
		private int code;

		/// <summary>
		/// Creates new has with predefined has code.
		/// </summary>
		/// <param name="code">Predefined hash code.</param>
		public FnvHash(int code) {
			this.code = code;
		}

		/// <summary>
		/// Creates new has with predefined has code.
		/// </summary>
		/// <param name="code">Predefined hash code.</param>
		public FnvHash(uint code) {
			unchecked {
				this.code = (int)code;
			}
		}

		/// <summary>
		/// Returns string reprepresentation of computed hash code.
		/// </summary>
		/// <returns></returns>
		public override string ToString() => this.code.ToString();

		/// <summary>
		/// Returns true if <paramref name="obj"/> is <see cref="FnvHash"/> and has equal hash code like this.
		/// </summary>
		/// <param name="obj">Compared hash.</param>
		/// <returns></returns>
		public override bool Equals(object obj) {
			if (obj is FnvHash hc) {
				return (this.code == hc.code);
			}
			else {
				return false;
			}
		}

		/// <summary>
		/// Returns computed hashcode.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() => this.code;

		/// <summary>
		/// Returns true if <paramref name="other"/> has equal hash code like this.
		/// </summary>
		/// <param name="other">Compared hash.</param>
		/// <returns></returns>
		public bool Equals(FnvHash other) => this.code == other.code;

		/// <summary>
		/// Basic hash code used for initialization of hash function.
		/// </summary>
		public const uint BaseCode = 2166136261;

		/// <summary>
		/// Signed variant of <see cref="BaseCode"/>.
		/// </summary>
		public static int SignedBaseCode {
			get {
				int result;
				unchecked { result = (int)BaseCode; }
				return result;
			}
		}

		/// <summary>
		/// Code used for extending hash code by another hash code.
		/// </summary>
		public const int NextCode = 16777619;

		/// <summary>
		/// Creates new initialized FnvHash.
		/// </summary>
		/// <returns></returns>
		public static FnvHash Init() => new FnvHash(BaseCode);

		/// <summary>
		/// Creates new initialized hash code extended by hash code from <paramref name="obj"/>.
		/// </summary>
		/// <typeparam name="T">Type of argument.</typeparam>
		/// <param name="obj">Argument used to extend initialized hash code.</param>
		/// <returns></returns>
		public static FnvHash Of<T>(T obj) => Init().And<T>(obj);

		/// <summary>
		/// Creates new initialized hash code extended by hash code from <paramref name="obj"/>.
		/// </summary>
		/// <typeparam name="T">Type of argument.</typeparam>
		/// <param name="obj">Argument used to extend initialized hash code.</param>
		/// <returns></returns>
		public static FnvHash Of<T>(ref T obj) => Init().And<T>(ref obj);

		/// <summary>
		/// Returns new <see cref="FnvHash"/> based on current hash extended by <paramref name="hash"/>.
		/// </summary>
		/// <param name="hash">Hash used to extend current hash.</param>
		/// <returns></returns>
		public FnvHash WithHash(int hash) {
			var next = (this.code * NextCode) ^ hash;
			return new FnvHash(next);
		}

		/// <summary>
		/// Returns new <see cref="FnvHash"/> based on current hash extended by <paramref name="hash"/>.
		/// </summary>
		/// <param name="hash">Hash used to extend current hash.</param>
		/// <returns></returns>
		public FnvHash WithHash(uint hash) {
			unchecked {
				var next = (int)((this.code * NextCode) ^ hash);
				return new FnvHash(next);
			}
		}

		/// <summary>
		/// Returns new <see cref="FnvHash"/> extended by hash code from <paramref name="obj"/>.
		/// </summary>
		/// <typeparam name="T">Type of argument.</typeparam>
		/// <param name="obj">Object used to extend current hash.</param>
		/// <returns></returns>
		public FnvHash And<T>(T obj) {
			if (obj is ValueType) {
				var hash = obj.GetHashCode();
				return WithHash(hash);
			}
			else if (ReferenceEquals(obj, null)) {
				return this;
			}
			else {
				var hash = obj.GetHashCode();
				return WithHash(hash);
			}
		}

		public FnvHash And<T0, T1>(T0 arg0, T1 arg1) => this.And(arg0).And(arg1);
		public FnvHash And<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2) => this.And(arg0, arg1).And(arg2);
		public FnvHash And<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3) => this.And(arg0, arg1, arg2).And(arg3);
		public FnvHash And<T0, T1, T2, T3, T4>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4) => this.And(arg0, arg1, arg2, arg3).And(arg4);
		public FnvHash And<T0, T1, T2, T3, T4, T5>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5) => this.And(arg0, arg1, arg2, arg3, arg4).And(arg5);
		public FnvHash And<T0, T1, T2, T3, T4, T5, T6>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6) => this.And(arg0, arg1, arg2, arg3, arg4, arg5).And(arg6);
		public FnvHash And<T0, T1, T2, T3, T4, T5, T6, T7>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7) => this.And(arg0, arg1, arg2, arg3, arg4, arg5, arg6).And(arg7);

		/// <summary>
		/// Returns new <see cref="FnvHash"/> extended by hash code from <paramref name="obj"/>.
		/// </summary>
		/// <typeparam name="T">Type of argument.</typeparam>
		/// <param name="obj">Object used to extend current hash.</param>
		/// <returns></returns>
		public FnvHash And<T>(ref T obj) {
			if (obj is ValueType) {
				var hash = obj.GetHashCode();
				return And(hash);
			}
			else if (ReferenceEquals(obj, null)) {
				return this;
			}
			else {
				var hash = obj.GetHashCode();
				return And(hash);
			}
		}

		public FnvHash And<T0, T1>(ref T0 arg0, ref T1 arg1) => this.And(ref arg0).And(ref arg1);
		public FnvHash And<T0, T1, T2>(ref T0 arg0, ref T1 arg1, ref T2 arg2) => this.And(ref arg0, ref arg1).And(ref arg2);
		public FnvHash And<T0, T1, T2, T3>(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3) => this.And(ref arg0, ref arg1, ref arg2).And(ref arg3);
		public FnvHash And<T0, T1, T2, T3, T4>(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4) => this.And(ref arg0, ref arg1, ref arg2, ref arg3).And(ref arg4);
		public FnvHash And<T0, T1, T2, T3, T4, T5>(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4, ref T5 arg5) => this.And(ref arg0, ref arg1, ref arg2, ref arg3, ref arg4).And(ref arg5);
		public FnvHash And<T0, T1, T2, T3, T4, T5, T6>(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4, ref T5 arg5, ref T6 arg6) => this.And(ref arg0, ref arg1, ref arg2, ref arg3, ref arg4, ref arg5).And(ref arg6);
		public FnvHash And<T0, T1, T2, T3, T4, T5, T6, T7>(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4, ref T5 arg5, ref T6 arg6, ref T7 arg7) => this.And(ref arg0, ref arg1, ref arg2, ref arg3, ref arg4, ref arg5, ref arg6).And(ref arg7);
	}
}
