﻿using System;
using System.Collections.Generic;
using System.Text;
using Fnv;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Fnv {
	[TestClass]
	public class FnvHash_Tests {
		[TestMethod]
		public void FnvWorks() {
			var hc0 = FnvHash.Init();
			Assert.AreEqual(FnvHash.SignedBaseCode, hc0.GetHashCode());
			Assert.AreEqual(FnvHash.SignedBaseCode, hc0.And<object>(null).GetHashCode());

			var hc1 = hc0.And(45);
			Assert.AreNotEqual(hc0, hc1);
			Assert.AreNotEqual(hc0.GetHashCode(), hc1.GetHashCode());

			var hc1b = hc0.And(45);
			Assert.AreEqual(hc1, hc1b);
			Assert.AreEqual(hc1.GetHashCode(), hc1b.GetHashCode());

			var obj1 = new object();
			var obj2 = new object();

			var hc2 = hc1.And(obj1);
			var hc2b = hc1.And(obj1);
			var hc2c = hc1.And(ref obj1);
			var hc2d = hc1.And(obj2);
			var hc2e = hc1.And(ref obj2);

			Assert.AreEqual(hc2, hc2b);
			Assert.AreEqual(hc2, hc2c);

			Assert.AreNotEqual(hc2, hc2d);
			Assert.AreNotEqual(hc2, hc2e);
			Assert.AreEqual(hc2d, hc2e);

			var hc3 = hc1.And(obj1).And(obj2);
			var hc3b = hc1.And(obj1, obj2);
			var hc3c = hc1.And(ref obj1).And(ref obj2);
			var hc3d = hc1.And(ref obj1, ref obj2);

			Assert.AreNotEqual(hc1, hc3);
			Assert.AreEqual(hc3, hc3b);
			Assert.AreEqual(hc3, hc3c);
			Assert.AreEqual(hc3, hc3d);
		}
	}
}
